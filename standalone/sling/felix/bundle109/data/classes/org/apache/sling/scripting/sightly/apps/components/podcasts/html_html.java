/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.components.podcasts;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.java.compiler.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;



public final class html_html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_properties = bindings.get("properties");
Object _dynamic_resource = bindings.get("resource");
Collection var_collectionvar4_list_coerced$ = null;
out.write("<!DOCTYPE html>\n<html>\n<head>");
{
    Object var_includedresult0 = renderContext.call("include", "/apps/components/header.html", obj());
    out.write(renderContext.getObjectModel().toString(var_includedresult0));
}
out.write("</head>\n<body>\n\t<h1>");
{
    Object var_2 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_properties, "jcr:title"), "text");
    out.write(renderContext.getObjectModel().toString(var_2));
}
out.write("</h1> \n\t<p>");
{
    Object var_3 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_properties, "jcr:description"), "text");
    out.write(renderContext.getObjectModel().toString(var_3));
}
out.write("</p>\n\t");
{
    Object var_collectionvar4 = renderContext.getObjectModel().resolveProperty(_dynamic_resource, "listChildren");
    {
        long var_size5 = ((var_collectionvar4_list_coerced$ == null ? (var_collectionvar4_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar4)) : var_collectionvar4_list_coerced$).size());
        if (renderContext.getObjectModel().toBoolean(var_size5)) {
            out.write("<div>");
            if (var_collectionvar4_list_coerced$ == null) {
                var_collectionvar4_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar4);
            }
            long var_index6 = 0;
            for (Object item : var_collectionvar4_list_coerced$) {
                out.write("\n\t\t<a href=\"\" class=\"list-group-item\">\n\t\t\t<h4 class=\"list-group-item-heading\">");
                {
                    Object var_7 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "jcr:title"), "text");
                    out.write(renderContext.getObjectModel().toString(var_7));
                }
                out.write("</h4>\n\t\t\t<p class=\"list-group-item-text\">");
                {
                    Object var_8 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "jcr:description"), "text");
                    out.write(renderContext.getObjectModel().toString(var_8));
                }
                out.write("</p>\n\t\t</a>\n\t");
                var_index6++;
            }
            out.write("</div>");
        }
    }
    var_collectionvar4_list_coerced$ = null;
}
out.write("\n</body>\n</html>");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

